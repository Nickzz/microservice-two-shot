# Wardrobify

Team:

* Nick - hats
* LaTorya - shoes

## Design

## Shoes microservice

Shoe:
Manufacturer
Model_name
Color
Picture_URL
Bin ** foreign key


BinVO:
import_href
Close_name
Bin_number
Bin_size


Endpoints
1. List of Shoes:
    - GET /api/shoes/ : Allows me to get a list of all of the shoes
    - POST /api/shoes : Allows the open to create a new shoe

2. Shoe Details:
    - GET /api/shoes/<init:id>/ : I can get the details of a specific shoe by it's id
    - PUT /api/shoes<int:id>: You have the ability to update the details about a specific shoe
    - DELETE /api/shoes/<int:id>/: You have the option to delete a specific shoe

3. List of Bins:
    - GET /api/bins/: Get a list of all of the bins
    -POST /api/bins/: You have the option to create a new bin

4. Bin Details:
    - GET /api/bins/<int:id>/: Get details of what's in a specific bin
    - PUT /api/bins/<int:id>/: Update the bin details for a specific one
    - DELETE /api/bins/<int:id>/: The ability to delete a specific bin

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Model 1: LocationVO
This model is a value object of Location model in wardrobe.

Model 2: Hat
This model contains all information about hat and also has a foreign key to locationVO model.
