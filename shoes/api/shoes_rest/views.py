from django.shortcuts import get_object_or_404
from common.json import ModelEncoder
from .models import Shoe, BinVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json



# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        'import_href',
        'closet_name',
        'bin_number',
        'bin_size'
    ]

    def get_extra_data(self, o):
        return {'bin_number': o.bin_number}

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'picture_url',
        'id'
    ]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'picture_url',
        'id'
    ]
    encoders = {
            'bin': BinVODetailEncoder(),
        }


@require_http_methods(["GET", "POST"])
def api_list_bins(request):
    if request.method == "GET":
        bins = BinVO.objects.all()
        bins_data = [BinVODetailEncoder().encode(bin) for bin in bins]
        return JsonResponse({'bins': bins_data})

    elif request.method == 'POST':
        data = json.loads(request.body)
        bin = BinVO.objects.create(
            import_href=data.get('import_href'),
            closet_name=data.get('closet_name'),
            bin_number=data.get('bin_number'),
            bin_size=data.get('bin_size')
        )
        return JsonResponse({'bin': BinVODetailEncoder().encode(bin)}, status=201)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_bin_detail(request, id):
    bin = get_object_or_404(BinVO, id=id)

    if request.method == "GET":
        bin_data = BinVODetailEncoder().encode(bin)
        return JsonResponse({'bin': bin_data})

    elif request.method == "PUT":
        data = json.loads(request.body)
        for key, value in data.items():
            setattr(bin, key, value)
        bin.save()
        bin_data = BinVODetailEncoder().encode(bin)
        return JsonResponse({'bin': bin_data})

    elif request.method == "DELETE":
        bin.delete()
        return JsonResponse({"deleted": True})

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse({'shoes': list(shoes)}, encoder=ShoeListEncoder, safe=False)
    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
            shoe = Shoe.objects.create(
                manufacturer=data["manufacturer"],
                model_name=data["model_name"],
                color=data["color"],
                picture_url=data["picture_url"],
                bin_id=data.get('bin_id')
            )
            return JsonResponse({'shoe':ShoeDetailEncoder().encode(shoe)}, safe=False)
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoes(request, id):
    try:
        if request.method == "GET":
            shoe = Shoe.objects.get(id=id)
            shoe_data = ShoeDetailEncoder().encode(shoe)
            return JsonResponse({"shoe": shoe_data})
        elif request.method == "DELETE":
            shoe = Shoe.objects.get(id=id)
            shoe.delete()
            return JsonResponse({"deleted": True})
        else:
            return JsonResponse({"error": "Method not allowed"}, status=405)
    except Shoe.DoesNotExist:
        return JsonResponse({"message": "Shoe not found"}, status=404)
