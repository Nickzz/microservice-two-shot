from django.db import models

# Create your models here.

class BinVO(models.Model):
    import_href = models.CharField(max_length=400, unique = True)
    closet_name = models.CharField(max_length=400)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    model_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField()

    bin = models.ForeignKey(
        BinVO,
        # related_name='shoes',
        on_delete=models.CASCADE,
        null=True
    )
