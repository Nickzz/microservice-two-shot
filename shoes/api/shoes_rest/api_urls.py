from django.urls import path
from .views import api_list_shoes, api_show_shoes, api_list_bins, api_bin_detail


urlpatterns = [
    path('shoes/', api_list_shoes, name="api_list_shoes"),
    path('shoes/<int:id>/', api_show_shoes, name='api_show_shoes'),
    path('bins/', api_list_bins, name="api_list_bins"),
    path('bins/<int:id>/', api_bin_detail, name="api_bin_detail")

]
