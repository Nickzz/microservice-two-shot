from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from django.shortcuts import get_object_or_404
from .models import Hat, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number", "import_href"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style", "color", "id", "pic_url"]

    def get_extra_data(self, o):
        return {"location": str(o.location)}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style", "color", "pic_url", "location"]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    """
    Lists the hats.

    GET:
    Returns a dictionary with a single key "hats" which
    is a list of hats style name. Each entry in the list
    is a dictionary that contains the fabric of shoes and
    the style of shoes.

    {
        "hats": [
            {
                "href": URL to the hat,
                "fabric": hats' fabric,
                "style": "fedora",
                "color": "black",
                "id": id,
                "location": closet_name - section_number/shelf_number,
                "pic_url": picture url
            },
            ...
        ]
    }

    POST:
    Creates a hat resource and returns its details.
    {
        "fabric": "wool",
        "style": "fedora",
        "color": "black",
        "pic_url": "https://www.explorerhats.com/wp-content/uploads/DF42_BLK_3Q.jpg",
        "location": ["closet_name", "section_number", "shelf_number"]
    }

    """
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatListEncoder)
    else:
        content = json.loads(request.body)
        try:
            closet_name = content["location"][0]
            section_number = content["location"][1]
            shelf_number = content["location"][2]

            location = get_object_or_404(
                LocationVO,
                closet_name=closet_name,
                section_number=section_number,
                shelf_number=shelf_number,
            )

            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_hats(request, pk):
    """
    Returns the details for the Hat model specified
    by the pk parameter.

    This should return a dictionary with fabric, style,
    color, pic_url, and a dictionary for the location containing
    its name and href.

    {
        "fabric": fabric,
        "style":
        "color":
        "pic_url": the description of the conference,
        "location": {
            "closet_name": location's closet name,
            "section_number": the number of the wardrobe section,
            "shelf_number": the number of the shelf,
        }
    }
    """
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse({"hat": hat}, encoder=HatDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
