import React, { useState, useEffect } from 'react';

function ShoeForm() {
  const [formData, setFormData] = useState({
    manufacturer: '',
    model_name: '',
    color: '',
    picture_url: '',
    bin: ''
  });

  const [bins, setBins] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    async function loadBins() {
      try {
        const response = await fetch("http://localhost:8080/api/bins/");
        if (response.ok) {
          const data = await response.json();
          setBins(data.bins);
        } else {
          console.error("Failed to fetch bins", response.statusText);
        }
      } catch (error) {
        console.error("Failed to fetch bins", error);
      }
    }
    loadBins();

  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch('http://localhost:8080/api/shoes/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (!response.ok) {
        const errorMessage = await response.text();
        throw new Error(errorMessage || 'Failed to create shoe');
      }

      setFormData({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin: ''
      });
      setError('');
    } catch (error) {
      console.error('Failed to add shoe:', error.message);
      setError(error.message);
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            {[
              { name: "manufacturer", label: "Manufacturer" },
              { name: "model_name", label: "Model Name" },
              { name: "color", label: "Color" },
              { name: "picture_url", label: "Picture URL" },
              { name: "bin", label: "Choose a Bin"}
            ].map(field => (
              <div className="form-floating mb-3" key={field.name}>
                {field.name === 'bin' ? (
                  <select
                    value={formData.bin}
                    onChange={handleFormChange}
                    required
                    name="bin"
                    id="bin"
                    className="form-select"
                  >
                    <option value=""></option>
                    {bins.map(bin => (
                      <option key={bin} value={bin}>{bin}</option>

                    ))}
                  </select>
                ) : (
                  <input
                    value={formData[field.name]}
                    onChange={handleFormChange}
                    placeholder={field.label}
                    required
                    type="text"
                    name={field.name}
                    id={field.name}
                    className="form-control"
                  />
                )}
                <label htmlFor={field.name}>{field.label}</label>
              </div>
            ))}
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
