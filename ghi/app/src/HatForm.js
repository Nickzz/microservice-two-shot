import React, { useState, useEffect } from 'react';

function HatForm() {
    // when the form comes up, should get data for locations from server
    // when form value is changed, should update the state
    // when form is submitted, should send data to server, and set state to empty
    const [locations, setLocations] = useState([]);
    const [formData, setFormData] = useState({
        fabric: '',
        style: '',
        color: '',
        pic_url: '',
        location: '',
    });

    const getData = async () => {
        const url = "http://localhost:8100/api/locations/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        //find the location in locations state whose id is same with the location id in the submitted form
        const selected = locations.find((loc) => loc.id === parseInt(formData.location));
        //turn the location state into a array
        formData.location = [selected.closet_name, selected.section_number, selected.shelf_number]
        const hatUrl = "http://localhost:8090/api/hats/";

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        try {
            const response = await fetch(hatUrl, fetchConfig);

            if (response.ok) {
                setFormData({
                    fabric: '',
                    style: '',
                    color: '',
                    pic_url: '',
                    location: ''
                });
                alert('Hat submitted successfully!');
            } else {
                const errorData = await response.json();
                console.error('Error submitting form:', errorData);
                alert(`Failed to submit hat: ${errorData.message}`);
            }
        } catch (error) {
            console.error('Failed to submit:', error);
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.fabric} placeholder="Name" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.style} placeholder="style" required type="text" name="style" id="style" className="form-control" />
                            <label htmlFor="style">style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.pic_url} placeholder="pic_url" required type="text" name="pic_url" id="pic_url" className="form-control" />
                            <label htmlFor="pic_url">pic_url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>{location.closet_name} - {location.section_number}/{location.shelf_number}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )

}

export default HatForm;
