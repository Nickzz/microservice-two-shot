import { useEffect, useState } from 'react';

function HatsList() {
    const [hatslists, setHats] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setHats(data.hats);
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const handleDelete = async (hatId) => {
        const url = `http://localhost:8090/api/hats/${hatId}`;

        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            //update the state immediately after deletion
            const responseHat = await fetch('http://localhost:8090/api/hats/');
            if (responseHat.ok) {
                const data = await responseHat.json();
                setHats(data.hats);
            }
            alert("Has deleted successfullly!")
        }
        else {
            const error = await response.json();
            alert('Failed');
        }

    }

    return (
        <table className="table table-striped table align-middle" style={{ width: '100%', tableLayout: 'fixed' }}>
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody >
                {hatslists.map(hat => {
                    return (
                        <tr style={{ height: '100px' }} key={hat.href}>
                            <td>{hat.fabric}</td>
                            <td>{hat.style}</td>
                            <td>{hat.color}</td>
                            <td>{hat.location}</td>
                            <td><img
                                src={hat.pic_url}
                                className="card-img-top img-fluid w-75"
                                alt='picture of the hat'
                            /></td>
                            <td>
                                <button onClick={() => handleDelete(hat.id)}>Delete</button>
                            </td>
                        </tr>

                    );
                })}
            </tbody>
        </table>
    )

}
export default HatsList;
