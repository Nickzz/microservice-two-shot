import React from 'react';
import { Routes, Route, Outlet, Link } from 'react-router-dom';
import HatsList from './HatsList';
import HatForm from './HatForm';

function HatsPage() {
    return (
        <div>
            <Link to="/hats/new" className="btn btn-outline-info btn-sm mt-3" >Create New Hat</Link>
            <Outlet />
        </div>

    )
}

export default HatsPage;
