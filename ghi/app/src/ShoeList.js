import React, { useState, useEffect } from 'react';

function ShoeList() {
  const [shoes, setShoes] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    async function loadShoes() {
      try {
        const response = await fetch("http://localhost:8080/api/shoes/");
        if (response.ok) {
          const data = await response.json();
          setShoes(data.shoes);
        } else {
          console.error("Failed to fetch shoes", response.statusText);
        }
      } catch (error) {
        console.error("Failed to fetch shoes", error);
      }
    }
    loadShoes();
  }, []);

  const handleDelete = async (id) => {
    try {
      const response = await fetch(`http://localhost:8080/api/shoes/${id}/`, {
        method: 'DELETE'
      });

      if (!response.ok) {
        const errorMessage = await response.text();
        throw new Error(errorMessage || 'Failed to delete shoe');
      }

      setShoes(prevShoes => prevShoes.filter(shoe => shoe.id !== id));
    } catch (error) {
      console.error('Failed to delete shoe:', error.message);
      setError(error.message);
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Model Name</th>
          <th>Manufacturer</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map((shoe) => (
          <tr key={shoe.id}>
            <td>{shoe.model_name}</td>
            <td>{shoe.manufacturer}</td>
            <td>{shoe.color}</td>
            <td><img src={shoe.picture_url} alt={shoe.model_name} style={{width: '200px', height: '200px'}} /></td>
            <td><button onClick={() => handleDelete(shoe.id)} className="btn btn-primary">Delete</button></td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default ShoeList;
